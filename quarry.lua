-- FastQuarry by Uli

local q = {}

-- Go forward once killing / breaking what is needed
function q.forward()
  -- TODO: Make more efficent
  while turtle.digUp()   do end
  while turtle.digDown() do end
  while turtle.dig()     do end

  while not turtle.forward() do
    while turtle.attack() do end
    turtle.dig()
  end
end

function q.turn(turnFn)
  turnFn()
  q.forward()
  turnFn()
end

function q.quarry_layer(length, width)
  for i=1,width do
    for j=1,length do
      q.forward()
    end

    if i % 2 == 0
      then q.turn(turtle.turnLeft)
      else q.turn(turtle.turnRight)
    end
  end

  -- Go to the beginning linewise
  -- TODO: Find better way to write this, there must be a more elegent (and fast) way
  if width % 2 == 1 then
    local i = length
    while i ~= 0 do
      if turtle.back() then
        i = i - 1
      else
        -- We ran into something! Turn around and attack & dig through.
        for _=1,2 do turtle.turnRight() end
        for _=i,0,-1 do
          q.forward()
        end

        turtle.turnRight()
        return
      end
    end

    turtle.turnLeft()
  end

  for i=1,width do
    q.forward()
  end

  turtle.turnRight()
end

function q.quarry(length, width)
  local depth = 0

  -- TODO: Check fuel.
  local bedrock = false
  while not bedrock do
    q.quarry_layer(length, width)

    turtle.digDown()
    -- TODO bedrock check verification
    while not turtle.down() do
      if not turtle.digDown() and not turtle.attack() then
        bedrock = true
        break
      end
    end

    depth = depth + 1
  end

  for i=1,depth do
    turtle.digUp()
    while not turtle.up() do
      turtle.digUp()
      turtle.attack()
    end
  end
end

function q.main(args)
  if #args < 2 then
    print('Usage: fastquarry <length> <width>')
  else
    local length = assert(tonumber(args[1]), 'length must be a number')
    local width  = assert(tonumber(args[2]), 'width must be a number')
    q.quarry(length, width)
  end
end

if testing123 then
  return q
else
  q.main({...})
end
